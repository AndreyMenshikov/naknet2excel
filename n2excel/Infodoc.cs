﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlRoot("Infodoc")]
    public partial class Infodoc
    {
        [XmlAttribute]
        public String Version;
        [XmlAttribute]
        public String Type;
        [XmlAttribute]
        public String Date;
        [XmlAttribute]
        public String Vendor;

        [XmlElement("Partner")]
        public Partner Partner;

        [XmlArray("Agents")]
        [XmlArrayItem("Agent")]
        public List<Agent> Agents;

        [XmlArray("Retails")]
        [XmlArrayItem("Retail")]
        public List<Retail> Retails;

        [XmlElement("Sales")]
        public Sales AllSales;

        [XmlElement("Stocks")]
        public Stocks AllStocks;
    }
}
