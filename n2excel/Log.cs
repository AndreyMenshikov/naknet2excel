﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaknetData
{
    public static class Log
    {
        static System.IO.StreamWriter file;
        static Log()
        {
            String workDir = Environment.CurrentDirectory;
            file = new System.IO.StreamWriter(workDir + "\\log.txt", true);
        }

        public static void writeLog(String log)
        {
            file.WriteLine(log);
            file.Flush();
        }
    }
}
