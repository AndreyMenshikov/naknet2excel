﻿using NaknetData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace n2excel
{
    class Program
    {
        static void Main(string[] args)
        {
            
            for(int argIndex = 0; argIndex < args.Length; argIndex++)
            {
                String arg = args[argIndex];
                if(arg == "--to-excel")
                {
                    toExcel();
                }
                else if(arg == "--to-naknet")
                {
                    if(argIndex + 1 < args.Length && args[argIndex+1].Length > 0)
                    {
                        String excelFile = args[argIndex + 1];

                        ExcelToNaknet convertor = new ExcelToNaknet();
                        convertor.convert(excelFile);
                    }
                } else
                {
                    System.Console.Out.WriteLine("Ошибка в аргументах");
                    System.Console.Out.WriteLine("Использовать: nconv --to-excel | --to-naknet <filename.xlsx>");
                }
            }
        }

        static void toExcel()
        {
            String workPath = Environment.CurrentDirectory;
            DirectoryInfo workDir = new DirectoryInfo(workPath);
            FileInfo[] files = workDir.GetFiles("*.xml");

            foreach (FileInfo fi in files)
            {
                loadXml(fi.FullName);
            }
        }

        static void loadXml(string file)
        {

            XmlTextReader sr = new XmlTextReader(file);
            sr.Namespaces = false;
            var serializer = new XmlSerializer(typeof(Infodoc));
            var doc = serializer.Deserialize(sr);
            sr.Close();
        }
    }
}
