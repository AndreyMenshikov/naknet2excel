﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlType("Retail")]
    public class Retail
    {
        [XmlAttribute]
        public String Id;

        [XmlAttribute]
        public String Inn;

        [XmlAttribute]
        public String Kpp;

        [XmlAttribute]
        public String Name;

        [XmlAttribute]
        public String City;

        [XmlAttribute]
        public String Address;
    }
}
