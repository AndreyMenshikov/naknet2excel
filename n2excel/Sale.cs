﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlType("Sale")]
    public class Sale
    {
        [XmlAttribute]
        public String LocalCode;

        [XmlAttribute]
        public String Code;

        [XmlAttribute]
        public String Barcode;

        [XmlAttribute]
        public String Art;

        [XmlAttribute]
        public String Qty;

        [XmlAttribute]
        public String Date;

        [XmlAttribute]
        public String RetailId;

        [XmlAttribute]
        public String AgentId;

        [XmlAttribute]
        public String Return;

        [XmlAttribute]
        public String Price;
    }
}
