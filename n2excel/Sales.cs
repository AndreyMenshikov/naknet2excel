﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlType("Sales")]
    public class Sales
    {
        [XmlAttribute]
        public String DateFrom;

        [XmlAttribute]
        public String DateTo;

        [XmlArrayItem("Sale")]
        public List<Sale> sales;
    }
}
