﻿using System;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlType("Stock")]
    public class Stock
    {
        [XmlAttribute]
        public String LocalCode;

        [XmlAttribute]
        public String Code;

        [XmlAttribute]
        public String Barcode;

        [XmlAttribute]
        public String Art;

        [XmlAttribute]
        public String Name;

        [XmlAttribute]
        public String Qty;

        [XmlAttribute]
        public String Date;
    }
}
