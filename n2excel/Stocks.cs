﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NaknetData
{
    [Serializable]
    [XmlType("Stocks")]
    public class Stocks
    {
        [XmlAttribute]
        public String DateFrom;

        [XmlAttribute]
        public String DateTo;

        [XmlArrayItem("Stock")]
        public List<Stock> stocks;

    }
}
